Imports Gizmox.WebGUI.Forms
Imports Gizmox.WebGUI.Common

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Write
    Inherits Gizmox.WebGUI.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Visual WebGui Designer
    Private Shadows components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Visual WebGui Designer
    'It can be modified using the Visual WebGui Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.Label1 = New Gizmox.WebGUI.Forms.Label()
		Me.SuspendLayout()
		'
		'Label1
		'
		Me.Label1.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill
		Me.Label1.Font = New System.Drawing.Font("Segoe UI", 72.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.Location = New System.Drawing.Point(0, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(663, 434)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "Please wait..."
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'Write
		'
		Me.Controls.Add(Me.Label1)
		Me.Size = New System.Drawing.Size(886, 425)
		Me.Text = "Write"
		Me.WindowState = Gizmox.WebGUI.Forms.FormWindowState.Maximized
		Me.ResumeLayout(False)

	End Sub
	Friend WithEvents Label1 As Gizmox.WebGUI.Forms.Label

End Class