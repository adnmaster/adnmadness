﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="AppHost._Default" %>

<%@ Register Assembly="Gizmox.WebGUI.Forms" Namespace="Gizmox.WebGUI.Forms.Hosts"
    TagPrefix="FormView" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Smooth Message</title>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name='og:image' content='/assets/social-share.jpg'>
    <link href='/assets/favicon.png' rel='icon' type='image/png'>
    <link rel='stylesheet' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
			<script src='//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js'></script>
			<script src='//oss.maxcdn.com/respond/1.4.2/respond.min.js'></script>
		<![endif]-->
    <link href='/assets/base.css' rel='stylesheet' type='text/css'>
    <link href='/assets/demo.css' rel='stylesheet' type='text/css'>
</head>
<body>
    <form id="Form1" runat="server">
    <nav class="rainbow-static navbar navbar-fixed-top" role="navigation">
	    	<div class="container">
				<div class="brand"><a href="/"><span><b>smooth</b>editor</span></a><span>Default.vb</span></div>
							</div>
		</nav>
    <div class="aloha-3d aloha-ui">
        <div class="rainbow-static aloha-link-toolbar btn-toolbar" role="toolbar">
            <input type="text" name="href" placeholder="http://www.alohaeditor.org" value="">
            <a class="aloha-link-follow btn glyphicon glyphicon-share-alt" href="#" rel="noreferrer"
                title="Start Application"></a>
        </div>
    </div>
    <div class="editable" style="text-align: center">
        <h3>
            Put your Subject &amp; Object here</h3>
        <p>
            Put your message here... No, just kidding!
            <br />
            This app allows you to send <i>public</i> messages to any person and to any device.
        </p>
        <p>
            <asp:HyperLink NavigateUrl="~/Read.wgx" runat="server" Text="Read" />
          &nbsp;|
            <asp:HyperLink  NavigateUrl="~/Write.wgx" runat="server" Text="Write" />
        </p>
        <p>
            Seriously.
            <br />
            <asp:HyperLink NavigateUrl="http://goo.gl/nwUGU3" runat="server" Text="Mohamed Amine SEBBANE" />
        </p>
    </div>
    <script type="text/javascript">        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-17460389-2', 'auto'); ga('require', 'displayfeatures');
        ga('send', 'pageview');</script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="http://www.alohaeditor.org/download/aloha.min.js"></script>
    <script src="js/aloha-ui-links.js"></script>
    <script src="/assets/background.js"></script>
    <script>
        (function () {
            'use strict';
            var commands = aloha.maps.merge(aloha.linksUi.commands, aloha.ui.commands);
            var editables = aloha.dom.query('.editable', document).map(aloha);
            for (var selector in commands) {
                $('.aloha-action-' + selector)
						.on('click', aloha.ui.command(editables, commands[selector]));
            }
            function middleware(event) {
                $('.aloha-ui .active, .aloha-ui.active').removeClass('active');
                if ('leave' === event.type) {
                    return event;
                }
                var states = aloha.ui.states(commands, event);
                for (var selector in states) {
                    var $item = $('.aloha-action-' + selector).toggleClass('active', states[selector]);
                    if (states[selector] && $item.parents('.dropdown-menu').length) {
                        $item.closest('.btn-group').find('.dropdown-toggle')
							     .addClass('active')[0].firstChild.data = $item[0].textContent + ' ';
                    }
                }
                return event;
            }
            aloha.editor.stack.unshift(aloha.linksUi.middleware, middleware);

            // Because Bootstrap dropdowm menu's use anchor tags containing
            // "href='#'" which causes the page to jump to the top
            $('.aloha-ui .dropdown-menu').on('click', function (event) { event.preventDefault(); });
        } ());
    </script>
    </form>
</body>
</html>
